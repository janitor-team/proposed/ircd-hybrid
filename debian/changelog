ircd-hybrid (1:8.2.39+dfsg.1-2) unstable; urgency=medium

  * Update debconf translations (Closes: #1003288)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 17 Apr 2022 19:37:33 +0100

ircd-hybrid (1:8.2.39+dfsg.1-1) unstable; urgency=medium

  * Update debconf translations (Closes: #987228, #994051)
  * New upstream release
  * Correct errors in ircd.conf (Closes: #999586)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 17 Apr 2022 19:27:26 +0100

ircd-hybrid (1:8.2.38+dfsg.1-2) unstable; urgency=medium

  * Update debconf translations (Closes: #982558, #982317, #983111, #983415)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 28 Feb 2021 12:22:17 +0000

ircd-hybrid (1:8.2.38+dfsg.1-1) unstable; urgency=medium

  * New upstream release
    - update config file for new reference.conf
    - add additional config directive renaming in postinst
  * Update NEWS with information about configuration directive renames
    and upgrade version constraints
  * Fix post-inst helper scripts to be executable
  * Remove reference to old version 7.2.2 in default MOTD

 -- Dominic Hargreaves <dom@earth.li>  Sun, 07 Feb 2021 15:51:55 +0000

ircd-hybrid (1:8.2.37+dfsg.1-1) unstable; urgency=medium

  * Update German debconf translation (Closes: #977152)
  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Mon, 04 Jan 2021 11:49:55 +0000

ircd-hybrid (1:8.2.36+dfsg.1-1) unstable; urgency=medium

  * Update German debconf translation (Closes: #960782)
  * Update Spanish debconf translation (Closes: #961914)
  * Update Portuguese debconf translation (Closes: #962404)
  * Update Dutch debconf translation (Closes: #963009)
  * Update Homepage to primary (.org) domain
  * New upstream release
    - update debian/copyright for file renames
    - update config file from new reference.conf
  * Update postinst logic to cater for more config renames
  * Add Breaks on older versions of anope which are not compatible
    with this release

 -- Dominic Hargreaves <dom@earth.li>  Sun, 06 Dec 2020 22:15:03 +0000

ircd-hybrid (1:8.2.31+dfsg.1-1) unstable; urgency=medium

  * New upstream release
    - update debian/copyright for new years and third party files
    - update config file from new reference.conf
    - update versioned dependency on gnutls and its configuration flag 
    - fix FTBFS with gcc-10 (Closes: #957377)
  * Add postinst logic to automatically rename config variables that
    have changed in this release
  * Bump debhelper compatibility level
  * Remove /etc/default/ircd-hybrid to fix Lintian error
    init.d-script-should-always-start-service

 -- Dominic Hargreaves <dom@earth.li>  Sun, 10 May 2020 23:50:34 +0100

ircd-hybrid (1:8.2.26+dfsg.1-1) unstable; urgency=medium

  * Remove libgeoip-dev from Build-Depends as this feature is no longer
    supported
  * Remove obsolete upgrade logic in maintainer scripts
  * New upstream release
    - update config file from new reference.conf
    - update debian/copyright for new years and third party files
  * Use dhparam.pem to avoid segfault on startup (Closes: #932774)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 01 Sep 2019 12:18:31 +0100

ircd-hybrid (1:8.2.24+dfsg.1-1) unstable; urgency=medium

  * New upstream release
    - update config file from new reference.conf
  * d/rules: Remove reference to non-existence get-orig-source target
  * Update Standards-Version
  * Don't run chown in recursive mode in maintainer scripts, fixing
    Lintian check maintainer-script-should-not-use-recursive-chown-or-chmod
  * Update Vcs-* fields to salsa

 -- Dominic Hargreaves <dom@earth.li>  Fri, 06 Apr 2018 22:58:00 +0100

ircd-hybrid (1:8.2.22+dfsg.1-1) unstable; urgency=medium

  * New upstream release
    - update config file from new reference.conf
  * Update Standards-Version (no changes)
  * Update Vcs-* fields to https

 -- Dominic Hargreaves <dom@earth.li>  Wed, 23 Aug 2017 00:23:03 +0100

ircd-hybrid (1:8.2.21+dfsg.1-1) unstable; urgency=medium

  * New upstream release
    - update config file from new reference.conf
  * Add Depends on lsb-base (thanks, Lintian)

 -- Dominic Hargreaves <dom@earth.li>  Sat, 03 Dec 2016 12:34:40 +0000

ircd-hybrid (1:8.2.19+dfsg.1-1) unstable; urgency=medium

  * New upstream release
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 28 Aug 2016 23:55:02 +0100

ircd-hybrid (1:8.2.17+dfsg.1-1) unstable; urgency=medium

  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Sat, 23 Apr 2016 13:05:40 +0100

ircd-hybrid (1:8.2.16+dfsg.1-2) unstable; urgency=high

  * Remove reference to obsolete debconf question, fixing
    uninstallability; thanks to Andreas Beckmann (Closes: #821823)

 -- Dominic Hargreaves <dom@earth.li>  Tue, 19 Apr 2016 23:13:39 +0100

ircd-hybrid (1:8.2.16+dfsg.1-1) unstable; urgency=medium

  * Add Brazilian Portuguese translation, thanks to Adriano Rafael Gomes
    (Closes: #816931)
  * New upstream release
    - update config files from new reference.conf
  * Build unconditionally with SSL support using new GnuTLS support
    (Closes: #697376)
  * Add NEWS entry about Anope in Debian and remove outdated debconf
    prompt about lack of services support
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 03 Apr 2016 13:33:01 +0100

ircd-hybrid (1:8.2.12+dfsg.1-1) unstable; urgency=medium

  * New upstream release
    - update debian/copyright with new copyright year
  * Add NEWS item about change requiring whole-network upgrade

 -- Dominic Hargreaves <dom@earth.li>  Sun, 17 Jan 2016 19:04:02 +0000

ircd-hybrid (1:8.2.11+dfsg.1-1) unstable; urgency=medium

  * New upstream release
    - update config files from new reference.conf
  * Add NEWS item about updated configuration and incompatible changes
  * Bump debhelper compatibility level
  * Various cleanups to debian/rules
  * Create /var/cache/ircd-hybrid for flattened links file

 -- Dominic Hargreaves <dom@earth.li>  Sat, 02 Jan 2016 15:16:00 +0000

ircd-hybrid (1:8.2.8+dfsg.1-3) unstable; urgency=medium

  * Correctly remove /var/lib/ircd-hybrid when purging package
    (Closes: #798372)

 -- Dominic Hargreaves <dom@earth.li>  Tue, 08 Sep 2015 22:08:29 +0100

ircd-hybrid (1:8.2.8+dfsg.1-2) unstable; urgency=medium

  * Fix typo in build date format
  * Fix regression in previous release where the server would not
    get correctly restarted after upgrade

 -- Dominic Hargreaves <dom@earth.li>  Sat, 05 Sep 2015 23:08:39 +0100

ircd-hybrid (1:8.2.8+dfsg.1-1) unstable; urgency=medium

  * New upstream release
    - update config files from new reference.conf 
  * Reproducibility fixes
    - Don't use the current time when building
    - Set file modes reproducibly 
  * Support status argument to init script
  * Store data files in /var/lib/ircd-hybrid instead of /etc/ircd-hybrid
    (Closes: #782892)
  * Remove obsolete --enable-halfops configure option
  * Sync debian/control.in with debian/control
  * Add Suggests: anope
  * Add a safeguard to stop debian/control getting out of sync
  * Remove obsolete build-dependencies on zlib1g-dev and libpcre3-dev
  * Remove reference to outdated m_opme patches in README.Debian
  * Don't create obsolete database files in /etc
  * Thanks to Michael Wobst for review of many of these changes

 -- Dominic Hargreaves <dom@earth.li>  Sat, 05 Sep 2015 22:35:51 +0100

ircd-hybrid (1:8.2.7+dfsg.1-1) unstable; urgency=medium

  * Remove Suggests: hybserv since it doesn't really work with
    ircd-hybrid 8 and above
  * New upstream release
    - update debian/copyright with minor changes
    - update config files from new reference.conf
    - fixes DoS from localhost clients (Closes: #782859)
    - supports SSL certficate chaining (Closes: #769741)
  * Debconf configuration script no longer ignores the result of
    upgrade questions (Closes: #779082)
  * Don't display upgrade warnings on new installs (Closes: #782883)
  * Add NEWS item about updated configuration

 -- Dominic Hargreaves <dom@earth.li>  Sun, 19 Apr 2015 15:53:09 +0100

ircd-hybrid (1:8.2.0+dfsg.1-2) unstable; urgency=medium

  * Updated Swedish debconf translation (Closes: #761974)
  * Updated Dutch debconf translation (Closes: #763645)
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 19 Oct 2014 11:35:21 +0100

ircd-hybrid (1:8.2.0+dfsg.1-1) unstable; urgency=low

  * Update debian/prep-new-tarball to not fail when trying to
    remove files which don't exist, and use +dfsg rather than .dfsg
    (thanks, Lintian)
  * New upstream release
    - update debian/copyright with minor changes
    - update config files from new reference.conf
  * Add NEWS item about updated configuration
  * Correct default MOTD to refer to 8.2 rather than 8.1
  * Use install -d rather than mkdir to ensure permissions are set
    correctly in certain build environments (thanks, Lintian)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 07 Sep 2014 18:26:34 +0100

ircd-hybrid (1:8.1.17.dfsg.1-1) unstable; urgency=medium

  * New upstream release
    - update ircd.confs to reflect changes in upstream reference.conf
  * Correct default MOTD to refer to 8.1 rather than 8.0

 -- Dominic Hargreaves <dom@earth.li>  Mon, 05 May 2014 15:24:20 +0100

ircd-hybrid (1:8.1.16.dfsg.1-1) unstable; urgency=medium

  * Update Debconf translations (Closes: #744013)
  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Mon, 21 Apr 2014 18:51:51 +0100

ircd-hybrid (1:8.1.15.dfsg.1-1) unstable; urgency=medium

  * New upstream release
    - update default config file
  * Update Standards-Version (no changes)
  * Add warning when upgrading away from a version with SSL support
    (Closes: #736813)
  * Update Debconf translations (Closes: #741058, #741105, #741149,
    #741150, #741357, #741403, #741455, #741869, #741937)

 -- Dominic Hargreaves <dom@earth.li>  Tue, 01 Apr 2014 22:52:14 +0100

ircd-hybrid (1:8.1.13.dfsg.1-1) unstable; urgency=low

  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Wed, 08 Jan 2014 22:17:08 +0000

ircd-hybrid (1:8.1.12.dfsg.1-1) unstable; urgency=low

  * Remove unused GPL-1+ license paragraph in debian/copyright
  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Sun, 05 Jan 2014 16:39:13 +0000

ircd-hybrid (1:8.1.7.dfsg.1-1) unstable; urgency=low

  [ Christian Perrier ]
  * Debconf templates and debian/control reviewed by the debian-l10n-
    english team as part of the Smith review project. Closes: #710876
  * [Debconf translation updates]
  * Russian (Yuri Kozlov).  Closes: #714096
  * Portuguese (Américo Monteiro).  Closes: #714432
  * Swedish (Martin Bagge / brother).  Closes: #714647
  * Czech (Miroslav Kure).  Closes: #715137
  * German (Helge Kreutzmann).  Closes: #715197
  * Italian (Beatrice Torracca).  Closes: #715389
  * Japanese (Hideki Yamane (Debian-JP)).  Closes: #715432
  * Spanish; (Camaleón).  Closes: #715476
  * Polish (Michał Kułach).  Closes: #715514
  * French (Christian Perrier).  Closes: #716888

  [ Dominic Hargreaves ]
  * Correct validity of max_topic_length in default configuration
    (Closes: #712180)
  * Include (empty) kline.conf and dline.conf files to avoid harmless
    warnings about them being missing (Closes: #713871)
  * Don't check for START=yes in the 'stop' action of the init script
    (Closes: #718192)
  * Remove Tao-of-IRC.940110 in repack script, as there is no
    licensing info
  * New upstream release
    - update ircd.confs to reflect changes in upstream reference.conf
  * Build-Depend on automake rather than automake1.13, as the virtual
    automake1.13 package isn't always pulled in correctly
  * Build with libgeoip support
  * Source /lib/lsb/init-functions in init script (thanks, Lintian)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 08 Sep 2013 12:43:57 +0100

ircd-hybrid (1:8.0.9.dfsg.1-2) unstable; urgency=low

  * Upload to unstable (Closes: #701439)
  * Add warning about lack of compatible services (see #706417)
  * Run dh_autoreconf (Closes: #698089)
  * Don't build bundled libltdl (Closes: #698090)

 -- Dominic Hargreaves <dom@earth.li>  Sat, 01 Jun 2013 00:59:04 +0100

ircd-hybrid (1:8.0.9.dfsg.1-1) experimental; urgency=low

  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Sun, 19 May 2013 19:18:49 +0100

ircd-hybrid (1:8.0.7.dfsg.1-1) experimental; urgency=low

  * New upstream release
    - update ircd.confs to reflect changes in upstream example.conf 
  * Add warning in debian/NEWS and in package preinst/config about
    cryptlink support going away in 8.x (Closes: #698091)
  * Rename CRYPTLINKS.txt to README.SSL to reflect the fact that the
    encrypted links are not called cryptlinks in 8.x
  * Add information about why OpenSSL is not linked by default to
    README.SSL

 -- Dominic Hargreaves <dom@earth.li>  Tue, 16 Apr 2013 00:03:29 +0100

ircd-hybrid (1:8.0.6.dfsg.1-1) experimental; urgency=low

  * New upstream release
    - Fixed build on GNU/Hurd (Closes: #698087)
    - Improved logging of configuration file issues (Closes: #698027)
    - nick and topic lengths are now configurable via ircd.conf; remove
      --with-nicklen and --with-topiclen from debian/rules, and note
      change in NEWS.Debian
    - update ircd.confs to reflect changes in upstream example.conf
    - Linux RT signal support for notification of socket events has been
      dropped; remove --enable-rtsigio from debian/rules
    - [CVE-2013-0238] fix DoS in hostmask.c:try_parse_v4_netmask()
      (Closes: #699267)
  * Correct Vcs-Git URL
  * Add prep-new-tarball script to assist in the creation of dfsg tarball

 -- Dominic Hargreaves <dom@earth.li>  Sun, 03 Feb 2013 00:12:38 +0000

ircd-hybrid (1:8.0.4.dfsg.1-1) experimental; urgency=low

  * Switch to dpkg-source 3.0 (quilt) format
  * Update patchlevel automatically (was out of date)
  * Switch to debhelper 8
  * Remove obsolete sections of debian/rules
  * Add missing ${misc:Depends} to hybrid-dev Depends
  * Add build-indep and build-arch targets
  * Update Standards-Version to 3.9.4
  * Switch to git-dpm for patch management
  * Add debian/README.source describing patch management and dfsg
    tarball creation
  * New upstream release (Closes: #413600)
    - drop all patches other than patchlevel_debian. Note
      that #283738, which was previously fixed by warn_no_ssl_files,
      still exists, but is expected to be fixed by an upstream change
      soon.
    - m_services.c no longer hard-codes services name (Closes: #425202)
    - can_flood is now correctly enforced (Closes: #506402)
    - MAXCLIENTS is now a runtime configuration, so remove support for
      setting it in debian/rules
    - Install headers into /usr/include/ircd-hybrid-8
  * Update debian/copyright to format 1.0; licence is GPL-2+
  * Don't build contrib modules as they contain known buggy code
    and are not supported upstream
  * Refresh default ircd.conf files from upstream examples
    (Closes: #664661)
  * Set a default sid in ircd.conf, and add NEWS item about the need to
    set a unique sid in a network
  * Fix Lintian warning about deprecated use of chown in SSL postinst

 -- Dominic Hargreaves <dom@earth.li>  Sun, 13 Jan 2013 16:01:22 +0000

ircd-hybrid (1:7.2.2.dfsg.2-10) unstable; urgency=high

  * [CVE-2013-0238] fix DoS in hostmask.c:try_parse_v4_netmask()
    (Closes: #699267)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 03 Feb 2013 00:54:15 +0000

ircd-hybrid (1:7.2.2.dfsg.2-9) unstable; urgency=low

  * Change maintainer in debian/control.in too

 -- Dominic Hargreaves <dom@earth.li>  Thu, 01 Nov 2012 21:13:39 +0000

ircd-hybrid (1:7.2.2.dfsg.2-8) unstable; urgency=low

  * New maintainer (Closes: #548366)
  * Add Vcs-* headers
  * Really enable hardened build flags; thanks to Simon Ruderich
    (Closes: #657537)
  * Fix bug where only one command line option could be passed; thanks
    to Denis Sacchet (Closes: #506192)
  * In README.Debian, note that SSL isn't enabled by default
    (Closes: #523113)

 -- Dominic Hargreaves <dom@earth.li>  Thu, 01 Nov 2012 20:30:57 +0000

ircd-hybrid (1:7.2.2.dfsg.2-7) unstable; urgency=low

  * Non-maintainer upload.
  * Orphan package. As indicated in #548366, Joshua Kwan is no longer
    interested and there's no visible activity by Aurelien for a long
    time now.
  * Enable hardened build flags and fix missing format string (also
    forwarded and accepted upstream) (Closes: #657537)	 

 -- Moritz Muehlenhoff <jmm@debian.org>  Sat, 07 Apr 2012 21:34:25 +0200

ircd-hybrid (1:7.2.2.dfsg.2-6.3) unstable; urgency=low

  * NMU.
  * Merge Ubuntu diff:

  [ Colin Watson ]
  * Update 16_use_debian_pcre.dpatch to link dynamically with libpcre rather
    than statically (closes: #640559, LP: #749282).

  [ Bhavani Shankar ]
  * Create /var/run/ircd on start/restart rather than relying on the directory
    shipped in the package, since /var/run may be a tmpfs
    (closes: #519136, LP: #337715).
  * Prevent conflict with other ircds: remove Conflicts: ircd. inspircd and
    ircd-ratbox are the only current ircds that aren't explicitly conflicted
    with, and there are no file conflicts with those packages
    (closes: #511611, LP: #375619).

  [ gregor herrmann ]
  * Add $remote_fs to Required-{Start,Stop} in debian/ircd-hybrid.init
    (lintian error).

 -- gregor herrmann <gregoa@debian.org>  Tue, 20 Sep 2011 19:17:42 +0200

ircd-hybrid (1:7.2.2.dfsg.2-6.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix pending l10n issues. Debconf translations:
    - Spanish (Camaleón).  Closes: #584456
    - Danish (Joe Hansen).  Closes: #600029
    - Italian (Vincenzo Campanella).  Closes: #600438

 -- Christian Perrier <bubulle@debian.org>  Tue, 19 Oct 2010 07:21:33 +0200

ircd-hybrid (1:7.2.2.dfsg.2-6.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix integer underflow, apply patch for DSA-1980-1 by the security team.
    (Closes: #567192)

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 17 Feb 2010 12:40:41 +0100

ircd-hybrid (1:7.2.2.dfsg.2-6) unstable; urgency=low

  * Add previously forgotten Swedish debconf translation.
    (Closes: #492179)
  * Update 98_patchlevel_debian.dpatch with a more simple version.

 -- Aurélien GÉRÔME <ag@debian.org>  Tue, 16 Sep 2008 18:50:28 +0200

ircd-hybrid (1:7.2.2.dfsg.2-5) unstable; urgency=low

  * Bump Standards-Version to 3.8.0.
  * Update watch file to mangle version.
  * Debconf translation:
    + Swedish, thanks to Martin Bagge. (Closes: #492179)
  * Update 19_sslonly.dpatch to fix FTBFS when built with
    USE_OPENSSL=1. (Closes: #482630)
  * Update 98_patchlevel_debian.dpatch as usual.

 -- Aurélien GÉRÔME <ag@debian.org>  Tue, 16 Sep 2008 15:22:34 +0200

ircd-hybrid (1:7.2.2.dfsg.2-4) unstable; urgency=low

  * Change my email address in the Uploaders field.
  * Bump Standards-Version to 3.7.3.
  * Add the Homepage field to debian/control.
  * Fix copyright notice in debian/copyright.
  * Remove linda overrides, as linda is no longer used.
  * Fix make distclean targets in debian/rules clean target.
  * Reload ircd-hybrid after rotating logs to avoid logging to
    deleted files, thanks to Julien Cristau. (Closes: #431132)
  * Debconf translations:
    + Dutch, thanks to Bart Cornelis. (Closes: #423043)
    + Portuguese, thanks to Américo Monteiro. (Closes: #443655)
    + Finnish, thanks to Esko Arajärvi. (Closes: #477549)
    + Basque, thanks to Piarres Beobide. (Closes: #479383)
    + Vietnamese, thanks to Clytie Siddall. (Closes: #479729)
    + Galician, thanks to Jacobo Tarrio. (Closes: #479806)
    + Russian, thanks to Yuri Kozlov. (Closes: #480257)
  * Fix typo in debian/ircd-hybrid.README.Debian, thanks to Wang Chun.
    (Closes: #463722)
  * Add 19_sslonly.dpatch to allow setting a channel to be only joinable
    by SSL-users or people on localhost, thanks to Joshua Kwan.
    (Closes: #419934)
  * Update 98_patchlevel_debian.dpatch as usual.

 -- Aurélien GÉRÔME <ag@debian.org>  Tue, 13 May 2008 02:21:25 +0200

ircd-hybrid (1:7.2.2.dfsg.2-3) unstable; urgency=low

  * Add debconf German translation from Helge Kreutzmann.
    (Closes: #401427)
    + Fix my mistake, because I did not include the template
      previously.
  * Update 98_patchlevel_debian.dpatch as usual.

 -- Aurélien GÉRÔME <ag@roxor.cx>  Tue, 27 Feb 2007 14:04:42 +0100

ircd-hybrid (1:7.2.2.dfsg.2-2) unstable; urgency=low

  * Add debconf German translation from Helge Kreutzmann.
    (Closes: #401427)
    + That fixes the previous incorrect entry closing #401427, sorry
      for this.

 -- Aurélien GÉRÔME <ag@roxor.cx>  Tue, 27 Feb 2007 09:05:00 +0100

ircd-hybrid (1:7.2.2.dfsg.2-1) unstable; urgency=high

  * Mention Martin Zobel's previous modification of
    debian/control{,.in} files before the upload, replacing
    "Pre-Depends: ${misc:Depends}" by
    "Pre-Depends: debconf (>= 0.5) | debconf-2.0".
  * Remove non-free documentation in upstream tarball.
    (Closes: #390667)
  * Add debconf Czech translation from Miroslav Kure. (Closes: #401427)
  * Update 02_fhs_comply.dpatch: fix broken path for m_spoof contrib
    module. (Closes: #410873)
  * Use sourceforge generic URL in debian/rules.
  * Update 98_patchlevel_debian.dpatch as usual.

 -- Aurélien GÉRÔME <ag@roxor.cx>  Tue, 27 Feb 2007 01:40:55 +0100

ircd-hybrid (1:7.2.2.dfsg.1-3) unstable; urgency=high

  * Use rtsigio on all architectures, except alpha, kfreebsd-i386,
    kfreebsd-amd64, and hurd-i386. (Closes: #392649)
  * Add debconf Japanese translation from Hideki Yamane.
    (Closes: #400006)
  * Add "Pre-Depends: ${misc:Depends}" in debian/control{.in,} for
    ircd-hybrid to solve a lintian warning.
  * Add "Suggests: hybserv" in debian/control{.in,} for ircd-hybrid.
  * Remove fuzzy markers in debconf translations, since they resulted
    from me fixing a typo (s/GNU TLS/GNUTLS/g) in a previous upload.
  * Update 98_patchlevel_debian.dpatch as usual.

 -- Aurélien GÉRÔME <ag@roxor.cx>  Wed, 29 Nov 2006 03:53:58 +0100

ircd-hybrid (1:7.2.2.dfsg.1-2) unstable; urgency=high

  * Compile with rtsigio on any architectures. (Closes: #393196)
  * Update 98_patchlevel_debian.dpatch as usual.

 -- Aurélien GÉRÔME <ag@roxor.cx>  Sun, 15 Oct 2006 16:12:27 +0200

ircd-hybrid (1:7.2.2.dfsg.1-1) unstable; urgency=high

  * Fix upgrade path from sarge to etch: if we upgrade from a version
    strictly below 1:7.2.2-1, then we do not ask whether or not to
    restart ircd-hybrid on each upgrade. (Closes: #389402)
  * Move debconf note to ircd-hybrid.README.Debian. (Closes: #389057)
  * Remove non-free documentation in upstream tarball.
    (Closes: #390667)
  * Add debconf Czech translation from Miroslav Kure.
    (Closes: #389622)
  * Add debconf French translation from Thomas Huriaux.
    (Closes: #390906)
  * Update 02_fhs_comply.dpatch: create relative symlinks from
    /usr/share/ircd-hybrid/help/users/* to ../opers/*.
    Thanks to Julien BLACHE for spotting this.
  * Update 98_patchlevel_debian.dpatch.

 -- Aurélien GÉRÔME <ag@roxor.cx>  Sun,  8 Oct 2006 22:49:16 +0200

ircd-hybrid (1:7.2.2-2) unstable; urgency=high

  * Mention two disregards in the previous changelog:
    * Execute the unpatch rule after the clean rule.
    * Update or disable several patches which are not applicable
      anymore. See details in debian/patches/00list.
  * Update description in debian/control, since the documentation is
    not present on the homepage anymore.
  * Add 18_remove_unused_va_list.dpatch to fix FTBFS on Alpha
    architecture and therefore set urgency to high. (Closes: #385695)
  * Option --enable-rtsigio does not work on Alpha architecture,
    so disable it and use option --enable-epoll instead.
  * Add debian/ircd-hybrid.prerm with a debconf setting to allow or to
    disallow ircd-hybrid to be stopped before an upgrade. The default
    behaviour is to allow it to be stopped. (Closes: #255519)
    + Use dh_installinit -n in debian/rules, because we now manage the
      init.d debconf part ourselves.
    + Accordingly update:
      - debian/ircd-hybrid.postinst.in;
      - debian/ircd-hybrid.postrm.in;
      - debian/ircd-hybrid.templates.
    + Add debian/ircd-hybrid.prerm.
    + Update and rename debian/ircd-hybrid.config.in to
      debian/ircd-hybrid.config.
  * Run debconf-updatepo and update the French template.
  * Update 98_patchlevel_debian.dpatch.
  * Update debian/copyright with current date.
  * Clean debian/ircd-hybrid.init and make it LSB-compliant.

 -- Aurélien GÉRÔME <ag@roxor.cx>  Tue, 19 Sep 2006 21:14:57 +0200

ircd-hybrid (1:7.2.2-1) unstable; urgency=low

  * New co-maintainer.
  * Update to the latest Standards-Version.
  * New upstream major version.
  * Update upstream URL in debian/rules.
  * Fix debian/rules for new upstream build system.
  * Repackage original tarball without the following files:
    - lib/ => contained the source code of pcre3-6.3;
    - src/lex.yy.c => file generated by lex;
    - src/y.tab.h => file generated by yacc;
    - src/y.tab.c => file generated by yacc.
  * Update 02_fhs_comply.dpatch for new upstream FHS compliance.
  * Add 15_use_debian_flex_and_bison.dpatch to truly reuse flex and
    bison build-dependencies in debian/control which were not used
    anymore.
  * Add 16_use_debian_pcre.dpatch to remove upstream's unmaintained
    built-in pcre3-6.3 and to use Debian's maintained libpcre3-dev
    as build-dependency instead.
  * Remove manpages of dropped tools convertconf, convertilines,
    convertklines, and viconf.
  * Remove debian/ircd-hybrid.links for the same reason.
  * Update lintian overrides with new files in /etc/ircd-hybrid/.
  * Add debian/watch.
  * Add debconf Vietnamese translation from Clytie Siddall.
    (Closes: #312033)
  * Add debconf Czech translation from Miroslav Kure.
    (Closes: #314778)
  * Add debconf Swedish translation from Daniel Nylander.
    (Closes: #338627)
  * Add debconf Portuguese translation from Miguel Figueiredo.
    (Closes: #362987)
  * NMU proposed 14_fix_va_list.dpatch already merged upstream.
    (Closes: #300638)
  * SSL build failure fixed upstream. (Closes: #337912)
  * Add 17_warn_no_ssl_files.dpatch which emits a warning in the log
    file when there is not a SSL certificate file and/or a RSA private
    key file. (Closes: #283738)
  * Remove debian/ircd-hybrid.TODO, as IPv6 support is now enabled
    by default and as it does not disable IPv4-only support anymore.
    (Closes: #265406)
  * Resurrect 98_patchlevel_debian.dpatch.
  * Add debian/ircd-hybrid.default which supplies
    /etc/default/ircd-hybrid to avoid starting ircd-hybrid when
    invoked via init.d shell script. (Closes: #225185)
  * Rework default sample configuration in debian/ircd.conf.nossl
    and in debian/ircd.conf.ssl, because some options do not exist
    anymore and some others are located into other blocks.
  * Update default sample message of the day in ircd.motd and move it
    from 03_customize_motd to debian/ircd.motd.

 -- Aurélien GÉRÔME <ag@roxor.cx>  Fri, 01 Sep 2006 01:42:53 +0200

ircd-hybrid (1:7.0.3-3.1) unstable; urgency=low

  * NMU
  * Fix a segfault on ppc and amd64 in va_list (Closes: #300638)
    Add 14_fix_va_list.dpatch
  * Change $(PWD) to $(CURDIR) in debian/rules
  * Remove tools/vimotd and tools/viklines on clean target in debian/rules
  * Update FSF address in debian/copyright

 -- Julien Danjou <acid@debian.org>  Tue, 16 May 2006 17:22:44 +0200

ircd-hybrid (1:7.0.3-3) unstable; urgency=high

  * Patch work:
    - 13_client_ssl_multi_read.dpatch: Thanks to Stephen Shirley
      <diamond@nonado.net>, finally fix the SSL lag bug that has driven me
      and users loopy for a long time!

 -- Joshua Kwan <joshk@triplehelix.org>  Thu, 28 Apr 2005 22:32:03 -0700

ircd-hybrid (1:7.0.3-2) unstable; urgency=low

  * hybrid-dev Depends on libadns1-dev. closes: #277198
  * Clean up debian/rules a bit.
  * Reindent ircd.conf.nossl and ircd.conf.ssl, and comment out more things.
    - Default anti_spam_exit_message_time becomes 0 minutes.
    - All connect blocks and wildcard RESVs are commented out by default,
      except for the NickServ ones.
  * Add French translation of debconf templates from Jens Nachtigall.
    closes: #263504
  * Add Japanese translation of debconf templates from Hideki Yamane.
    closes: #281079
  * Improve CRYPTLINKS.txt a little bit.
  * Apply patch from Henrik Persson to allow SSL support to work with
    IPv6.
  * Apply patch from Andreas Jochens to fix compile with gcc-4.0.
    closes: #286445
  * Lowercase some leading letters in short descriptions.

 -- Joshua Kwan <joshk@triplehelix.org>  Wed, 19 Jan 2005 21:06:41 -0800

ircd-hybrid (1:7.0.3-1) unstable; urgency=high

  * New upstream release fixes crashes on /MODLOAD, /MODRESTART, et al.
  * Fix misgeneration of postinst script when USE_OPENSSL=1. Thanks
    Misha Nasledov <misha@debian.org>. (It would also nuke your cryptlink
    key if you already had one. Ouch!)
  * Update French translation of debconf templates, by Rémi Pannequin.
    (Closes: #256803)

 -- Joshua Kwan <joshk@triplehelix.org>  Tue, 29 Jun 2004 23:10:43 -0700

ircd-hybrid (1:7.0.2-1) unstable; urgency=medium

  * New upstream stable release.
    - Fixes a big desync bug in mode checking.
  * Patch work:
    - 08_dot_lang.dpatch: resynced
  * Update lintian override to cover the new xline.conf, cresv.conf, and
    nresv.conf.

 -- Joshua Kwan <joshk@triplehelix.org>  Sat, 19 Jun 2004 17:43:50 -0700

ircd-hybrid (1:7.0.1-1) unstable; urgency=low

  * New upstream stable release.
  * Patch work:
    - 02_fhs_comply.dpatch: resynced
    - 06_whois_wildcard.dpatch: merged
    - 08_dot_lang.dpatch: resynced
  * Install blank xline.conf, cresv.conf and nresv.conf into
    /etc/ircd-hybrid.
  * Mention USE_OPENSSL in CRYPTLINKS.txt. Very important step. Oops.
    Thanks Russell Steicke <russells@adelie.cx>. (Closes: #249135)
  * Use a stamp to prevent rebuilding debian/control and debian/changelog
    all the time. Thanks Birzan George Cristian <gcbirzan@sns.ro>.
    (Closes: #250518)

 -- Joshua Kwan <joshk@triplehelix.org>  Wed,  2 Jun 2004 23:07:32 -0700

ircd-hybrid (1:7.0-7) unstable; urgency=medium

  * Brown paper bag release. Finish alternate source building method
    and append a bogus changelog entry to debian/changelog so that apt
    does not prefer the version in the archive.

 -- Joshua Kwan <joshk@triplehelix.org>  Sat, 13 Mar 2004 16:52:25 -0800

ircd-hybrid (1:7.0-6) unstable; urgency=medium

  * License issues: do not enable SSL by default, but enable it to be
    built easily from this source package.
  * Include French debconf template translation from
    Remi Pannequin <remi.pannequin@laposte.net> (Closes: #235190)
  * Change Build-Depends to debhelper (>= 4.1.16) as per po-debconf(7)
  * Tweaked hybrid-dev long Description.

 -- Joshua Kwan <joshk@triplehelix.org>  Fri, 12 Mar 2004 23:00:26 -0800

ircd-hybrid (1:7.0-5) unstable; urgency=medium

  * Merge in SSL client code from hybrid7-bg.
  * Fix the regexp used in ircd-hybrid.config used to check whether
    servlink_path was set.
  * Because we only show the debconf note when servlink_path is set,
    make the debconf question medium priority.
  * Cosmetic change to make manpage generation clearer.
  * Relicense all of my manual pages as GPL. I previously used the GFDL
    template provided by dh_make.
  * Make it clear that mbuild-hybrid is GPL licensed.
  * mbuild-hybrid uses -O2 now by default.
  * Convert Debconf templates to po-debconf style.
  * Refer to the right logfile in README.Debian (closes: #225181)
  * config should bail out if it doesn't see an existing ircd.conf
    (closes: #225182)

 -- Joshua Kwan <joshk@triplehelix.org>  Mon, 16 Feb 2004 18:35:55 -0800

ircd-hybrid (1:7.0-4) unstable; urgency=high

  * Brown paper bag release.
  * Fix postrm, it would usually fail. Especially with no backslashes at the
    ends of lines...
  * Shut egrep up.
  * Bump Standards-Version to 3.6.1.0. No changes needed, it's all debconf
    here.

 -- Joshua Kwan <joshk@triplehelix.org>  Sat, 23 Aug 2003 16:22:49 -0700

ircd-hybrid (1:7.0-3) unstable; urgency=low

  * Update to Standards-Version 3.6.0. No changes were necessary.
  * Add shell parentheses to some of the backport lines to prevent
  some unintended execution. (Closes: #202415)
  * Remove docbook-to-man generated PostScript header junk from manpages.
  * Don't install RFC 1459 anymore.
  * Force /etc/ircd-hybrid ch{mod,own} in postinst to irc:irc, etc.
    Some combination of removals and installs somehow got this dir installed
    as root:root. I refuse to dwell on it.
  * As per discussion on debian-devel, made postrm purge a little less
    zealous.
  * Remove /var/log/ircd in postrm.
  * Fix the init script to support reload (SIGHUP) and make 'restart' work if
  the daemon is not already running.
  * Don't display the debconf note unless servlink_path is in the
    configuration file and uncommented.
  * chmod -x /usr/share/doc/ircd-hybrid/examples/simple.conf.
  * Use 'export CFLAGS' instead of $(MAKE) CFLAGS=whatever.

 -- Joshua Kwan <joshk@triplehelix.org>  Sun, 10 Aug 2003 17:32:12 -0700

ircd-hybrid (1:7.0-2) unstable; urgency=low

  * Backport the new .lang support!
  * Fix the get-orig-source target.
  * Use --enable-rtsigio, which is faster than the default of poll.
  * Fix a typo (woody backport only) in debian/rules.
  * Improve the descriptions.
  * Corrected upstream version in debian/copyright.
  * Removed debian/watch.
  * (From 7.0-1) Change section of hybrid-dev to devel.
  * IRCD_PREFIX turns out to be `pwd`/debian/tmp, so make DPATH "/usr".
  * Install example_module.c into examples.
  * Fix some nasty overlinkages; convertconf and viconf were getting linked
    with OpenSSL libraries (it is this way upstream as well...)

 -- Joshua Kwan <joshk@triplehelix.org>  Thu, 26 Jun 2003 02:00:22 -0700

ircd-hybrid (1:7.0-1) unstable; urgency=low

  * New upstream release. (Closes: #197069)
  * Don't build mkpasswd-hybrid's manpage anymore because there is
  no longer a corresponding binary.
  * Apply post-7.0 fix from Bill Jonus <bill@mu.org> to fix a wildcard
  user security issue in /whois.
  * libc6-dev is in build-essential, remove it from Build-Depends.

 -- Joshua Kwan <joshk@triplehelix.org>  Wed, 11 Jun 2003 13:11:48 -0700

ircd-hybrid (7.0rc10-1) unstable; urgency=low

  * New upstream release.
  * If /var/run/ircd and /var/log/ircd existed before, make sure they
    get chown'd to irc:irc in the postinst or the init script will mess
    up.
  * patch work:
        + 98_patchlevel_debian.dpatch is no longer part of the default
          build. Some people thought it was obnoxious...
  * TOPICLEN has a hard max of 390. Any higher value specified in the
    configure stage only wastes memory later. Thanks xxjack12xx@doramail.com
  * NICKLEN is now set to 15. Why? Take a look at this: (real excerpt)
    < yamsyamsyamsyamsyamsyamsy> hahaha i gayed up your client lololololol
    Checking on Freenode, most people who exceed the traditional NICKLEN=9
    don't do so by more than 4 or 5 characters.
  * Made MAXCLIENTS easily customizable in debian/rules.
  * Added a logrotate script.
  * Stop shipping a mkpasswd-hybrid binary. The real mkpasswd does everything
    it can do! This also removes the manpage, and rectifies the command
    for generating the passwords in ircd.conf (mkpasswd [-Hmd5])
  * This also means we Recommends: whois.
  * Add a linda override.
  * Added a debconf note that talks about what the admin should do
  with servlink_path.

 -- Joshua Kwan <joshk@triplehelix.org>  Wed,  7 May 2003 22:22:44 -0700

ircd-hybrid (7.0rc9-4) unstable; urgency=low

  * "Janitor work" release.
  * Bump Standards-Version to 3.5.9 and best debhelper practices:
  	+ The following programs now have manpages:
          mkpasswd-hybrid encspeed viconf viklines convertconf
          convertklines convertilines
  	+ DH_COMPAT=4 becomes echo 4 > debian/compat
  * debian/rules general mucking:
        + Removed old boilerplate 'package', 'prefix', 'INSTALL_PROGRAM'
          vars - they're not needed at all.
        + Added 'get-orig-source' target that uses wget.
  * new stuff:
        + debian/watch
        + mbuild-hybrid (hybrid-dev) - painless way to automagically
          build and install ircd-hybrid modules, and mbuild-hybrid.1,
          and mention it in hybrid-dev.README
        + contrib/README -> /u/s/d/i/README.contrib.gz
        + RELNOTES -> /u/s/d/i/changelog.gz. This means remove the entry
          from debian/ircd-hybrid.docs. Upstream's `ChangeLog' is way too
          big and really is a more long-winded RELNOTES, so it is not
          included.
  * patch work:
        + 02_fhs_comply.dpatch: servlink shouldn't be run by any user,
          make it in /usr/lib/ircd-hybrid. experimental for now; should
          not cause problems at all. Can be backed out later if it causes
          problems, but hasn't yet on a production box connected to two
          servers.
        + 03_opme_no_depend_opless.dpatch: now cleaner, removes the
          now-unused chan_is_opless function.
        + (NEW) 99_opme_allow_operators.dpatch: m_opme.c, allow operators too.
          the rationale is that if you can /kill people, you should be able to
          kick and ban people from channels too. NOT PART OF THE DEFAULT BUILD.
        + (NEW) 99_patchlevel_debian.dpatch: stand out from the others :)
  * README.Debian (ircd-hybrid.README):
        + notes effects of 03_opme_no_depend_opless.dpatch
        + notes effects of 99_opme_allow_operators.dpatch and that it is
          not part of the default build
        + noted that /set msglocale foo is broken but fixed in upstream
          development version
  * /etc/init.d/ircd-hybrid uses SIGTERM instead of SIGKILL for now to
    end ircd-hybrid's processes. It doesn't make any difference at the
    moment but it's always good to use less drastic measures when possible.
  * chmod -x usr/share/doc/ircd-hybrid/simple.conf - upstream braindamage?
  * Removed empty/bogus usr/share/ircd-hybrid/modules dir.
  * Default ircd.conf has autoconn disabled for the sample connect {}
    block and have_ident set to no.
  * Move example*.conf to examples; add simple.conf to examples.
  * Take out all the example conf files from docs.
  * Made it a little easier for woody people to build ircd-hybrid.
    (They still need to figure out the dpatch bdep on their own, though)
  * Added the missing gettext build-dependency for woody.
  * Run dh_md5sums for both packages.

 -- Joshua Kwan <joshk@triplehelix.org>  Sat,  5 Apr 2003 18:06:14 -0800

ircd-hybrid (7.0rc9-3) unstable; urgency=low

  * Make all logs in /var/log/ircd and make all pids in /var/run/ircd. 
    Making additional directories all the time is a pain in the ass.
  * Remove some of the unhelpful BS from ircd.conf

 -- Joshua Kwan <joshk@triplehelix.org>  Wed, 12 Mar 2003 12:45:54 -0800

ircd-hybrid (7.0rc9-2) unstable; urgency=low

  * Add build dependency for flex.

 -- Joshua Kwan <joshk@triplehelix.org>  Mon,  3 Mar 2003 20:51:59 -0800

ircd-hybrid (7.0rc9-1) unstable; urgency=low

  * Initial release.

 -- Joshua Kwan <joshk@triplehelix.org>  Sat,  1 Mar 2003 16:45:03 -0800
