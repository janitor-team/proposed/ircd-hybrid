# Debconf translations for ircd-hybrid.
# Copyright (C) 2016 THE ircd-hybrid'S COPYRIGHT HOLDER
# This file is distributed under the same license as the ircd-hybrid package.
# Adriano Rafael Gomes <adrianorg@arg.eti.br>, 2016-2021.
#
msgid ""
msgstr ""
"Project-Id-Version: ircd-hybrid\n"
"Report-Msgid-Bugs-To: ircd-hybrid@packages.debian.org\n"
"POT-Creation-Date: 2021-02-07 15:23+0000\n"
"PO-Revision-Date: 2021-04-11 14:58-0300\n"
"Last-Translator: Adriano Rafael Gomes <adrianorg@arg.eti.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../ircd-hybrid.templates:2001
msgid "Restart ircd-hybrid on each upgrade?"
msgstr "Reiniciar o ircd-hybrid a cada atualização?"

#. Type: boolean
#. Description
#: ../ircd-hybrid.templates:2001
msgid ""
"Please choose whether the ircd-hybrid daemon should be restarted every time "
"a new version of this package is installed."
msgstr ""
"Por favor, escolha se o daemon do ircd-hybrid deve ser reiniciado a cada vez "
"que uma nova versão desse pacote for instalada."

#. Type: boolean
#. Description
#: ../ircd-hybrid.templates:2001
msgid ""
"Automatic restarts may be problematic if, for instance, the server is "
"running with manually loaded modules, which will need to be reloaded after "
"the restart."
msgstr ""
"Reinícios automáticos podem ser problemáticos se, por exemplo, o servidor "
"estiver sendo executado com módulos carregados manualmente, os quais "
"precisarão ser recarregados depois do reinício."

#. Type: boolean
#. Description
#: ../ircd-hybrid.templates:2001
msgid ""
"If you reject this option, you will have to restart ircd-hybrid via "
"\"service ircd-hybrid restart\" when needed."
msgstr ""
"Se você rejeitar essa opção, você terá que reiniciar o ircd-hybrid via "
"\"service ircd-hybrid restart\" quando necessário."

#. Type: boolean
#. Description
#: ../ircd-hybrid.templates:3001
msgid "Automatically fix references to obsolete config options?"
msgstr ""
"Corrigir automaticamente referências a opções de configuração obsoletas?"

#. Type: boolean
#. Description
#: ../ircd-hybrid.templates:3001
msgid ""
"Several ssl configuration variables have been renamed to tls-like ones in "
"version 8.2.30, with some further changes to rename network_desc and "
"max_watch in 8.2.36, and dots_in_ident in 8.2.38. If enabled, the post-"
"installation script will attempt to automatically fix them before the server "
"is restarted. If not, and you have these options specified, the "
"configuration will become invalid, and server restart will fail."
msgstr ""
"Várias variáveis de configuração ssl foram renomeadas para equivalentes tls "
"na versão 8.2.30, com algumas modificações adicionais para renomear "
"network_desc e max_watch na 8.2.36, e dots_in_ident na 8.2.38. Se "
"habilitado, o script de pós-instalação tentará corrigi-las automaticamente "
"antes de o servidor ser reiniciado. Se não habilitado e você tiver essas "
"opções especificadas, a configuração se tornará inválida, e a "
"reinicialização do servidor falhará."

#~ msgid "Upgrade ircd-hybrid to version without cryptlink support?"
#~ msgstr "Atualizar o ircd-hybrid para a versão sem suporte a cryptlink?"

#~ msgid ""
#~ "The 8.x version of ircd-hybrid includes a change to the way secure server "
#~ "links are implemented, which is not backwards-compatible with ircd-hybrid "
#~ "7.x, from which you are upgrading."
#~ msgstr ""
#~ "A versão 8.x do ircd-hybrid inclui uma mudança no modo no qual os links "
#~ "de servidores seguros são implementados, que não mantém compatibilidade "
#~ "retroativa com o ircd-hybrid 7.x, a partir do qual você está atualizando."

#~ msgid ""
#~ "If you have any secure server links (cryptlinks) configured with this "
#~ "server, you should plan to either upgrade all servers in lock-step, or "
#~ "temporarily configure non-cryptlink server links, to ensure the "
#~ "continuity of your IRC links."
#~ msgstr ""
#~ "Se você tiver quaisquer links de servidores seguros (cryptlinks) "
#~ "configurados com esse servidor, você deverá planejar atualizar todos os "
#~ "servidores em conjunto, ou configurar temporariamente links de servidores "
#~ "sem cryptlink, para assegurar a continuidade dos seus links de IRC."
