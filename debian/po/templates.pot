# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the ircd-hybrid package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ircd-hybrid\n"
"Report-Msgid-Bugs-To: ircd-hybrid@packages.debian.org\n"
"POT-Creation-Date: 2021-02-07 15:23+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../ircd-hybrid.templates:2001
msgid "Restart ircd-hybrid on each upgrade?"
msgstr ""

#. Type: boolean
#. Description
#: ../ircd-hybrid.templates:2001
msgid ""
"Please choose whether the ircd-hybrid daemon should be restarted every time "
"a new version of this package is installed."
msgstr ""

#. Type: boolean
#. Description
#: ../ircd-hybrid.templates:2001
msgid ""
"Automatic restarts may be problematic if, for instance, the server is "
"running with manually loaded modules, which will need to be reloaded after "
"the restart."
msgstr ""

#. Type: boolean
#. Description
#: ../ircd-hybrid.templates:2001
msgid ""
"If you reject this option, you will have to restart ircd-hybrid via "
"\"service ircd-hybrid restart\" when needed."
msgstr ""

#. Type: boolean
#. Description
#: ../ircd-hybrid.templates:3001
msgid "Automatically fix references to obsolete config options?"
msgstr ""

#. Type: boolean
#. Description
#: ../ircd-hybrid.templates:3001
msgid ""
"Several ssl configuration variables have been renamed to tls-like ones in "
"version 8.2.30, with some further changes to rename network_desc and "
"max_watch in 8.2.36, and dots_in_ident in 8.2.38. If enabled, the post-"
"installation script will attempt to automatically fix them before the server "
"is restarted. If not, and you have these options specified, the "
"configuration will become invalid, and server restart will fail."
msgstr ""
