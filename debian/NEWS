ircd-hybrid (1:8.2.38+dfsg.1-1) unstable; urgency=medium

  ircd-hybrid 8.2.38 contains several configuration changes compared to 8.2.31
  and below. You are recommended to review your configuration against the
  defaults supplied with this package when upgrading, or automatically
  choose the option during upgrade to update your configuration to be
  compatible. The scripts in /usr/share/ircd-hybrid/scripts can be used to
  update your configuration later, if needed.

  This release cannot be used to link with any ircd-hybrid server < 8.2.23.
  If you have such servers on your network (including those using Debian
  packages from Debian 8, aka stretch) or below, you should upgrade those
  to at least 8.2.23 or Debian 9, aka buster) before upgrading this package.

  This release cannot be used to link with any Anope release < 2.0.8.
  For typical configurations where anope is running on the same host,
  this requirement is enforced through Debian package relationships. If you
  have a configuration where ircd-hyrid is linked to Anope running on a
  different system, ensure that this installation is sufficiently up-to-date
  before upgrading.

 -- Dominic Hargreaves <dom@earth.li>  Sun, 07 Feb 2021 15:25:04 +0000

ircd-hybrid (1:8.2.31+dfsg.1-1) unstable; urgency=medium

  ircd-hybrid 8.2.31 contains several configuration changes compared to 8.2.26
  and below. You are recommended to review your configuration against the
  defaults supplied with this package when upgrading, or automatically
  choose the option during upgrade to update your configuration to be
  compatible. The script /usr/share/ircd-hybrid/scripts/ssl-to-tls-config
  can be used to update your configuration later, if needed.

  The non-standard STARTS parameter in /etc/default/ircd-hybrid has
  been removed. Please use update-rc.d or systemctl as appropriate to
  control automatic starting of ircd-hybrid.

 -- Dominic Hargreaves <dom@earth.li>  Sun, 10 May 2020 23:50:34 +0100

ircd-hybrid (1:8.2.16+dfsg.1-1) unstable; urgency=medium

  This package now builds SSL/TLS support in by default, thanks to the
  new upstream support for GnuTLS.

  Since the release of Anope 2, Anope has been the recommended services
  for ircd-hybrid. This is now (as of May 2015) in Debian, meaning there
  is out of the box services support for ircd-hybrid in Debian again.

 -- Dominic Hargreaves <dom@earth.li>  Sun, 03 Apr 2016 12:38:50 +0100

ircd-hybrid (1:8.2.12+dfsg.1-1) unstable; urgency=medium

  Character case mapping is 'ascii' now. It is recommended
  to update all servers on the network to prevent possible channel
  desynchronizations and nick name collision kills

 -- Dominic Hargreaves <dom@earth.li>  Sun, 17 Jan 2016 18:48:50 +0000

ircd-hybrid (1:8.2.11+dfsg.1-1) unstable; urgency=medium

  ircd-hybrid 8.2.11 contains several configuration changes compared to 8.2.8
  and below. You are recommended to review your configuration against the
  defaults supplied with this package when upgrading.

  Incompatible changes: RSA keys with less than 2048 bits are no longer
  supported, and the CHALLENGE in favour or ssl certificate fingerprint
  enabled operator {} blocks

 -- Dominic Hargreaves <dom@earth.li>  Sat, 02 Jan 2016 12:36:37 +0000

ircd-hybrid (1:8.2.8+dfsg.1-1) unstable; urgency=medium

  This release moves database files from /etc to /var/lib, where they
  should be, and makes files in /etc/ircd-hybrid root-owned. You should
  check that things are as you expect after this change.

 -- Dominic Hargreaves <dom@earth.li>  Sat, 05 Sep 2015 21:47:36 +0100

ircd-hybrid (1:8.2.7+dfsg.1-1) unstable; urgency=medium

  ircd-hybrid 8.2.7 contains several configuration changes compared to 8.2.0
  and below. You are recommended to review your configuration against the
  defaults supplied with this package when upgrading.

 -- Dominic Hargreaves <dom@earth.li>  Sun, 19 Apr 2015 14:18:44 +0100

ircd-hybrid (1:8.2.0+dfsg.1-1) unstable; urgency=low

  ircd-hybrid 8.2 contains several configuration changes compared to 8.1
  and below. You are recommended to review your configuration against the
  defaults supplied with this package when upgrading.

 -- Dominic Hargreaves <dom@earth.li>  Sun, 07 Sep 2014 18:02:44 +0100

ircd-hybrid (1:8.0.7.dfsg.1-1) experimental; urgency=low

  ircd-hybrid 8.x includes a change to the way secure server links are
  implemented, which is not backwards-compatible with ircd-hybrid 7.x.

  If you have any secure server links (cryptlinks) configured, you should
  plan to either upgrade all servers in lock-step, or temporarily configure
  non-cryptlink server links, to ensure the continuity of your IRC links,
  before upgrading to ircd-hybrid 8.x.

 -- Dominic Hargreaves <dom@earth.li>  Sun, 14 Apr 2013 19:17:11 +0100

ircd-hybrid (1:8.0.6.dfsg.1-1) experimental; urgency=low

  Maximum NICK and TOPIC length are now configurable at runtime; the
  previous Debian defaults of 15 and 350 respectively have been removed
  in favour of the upstream defaults of 30 and 300. A max_nick_length, as
  well as a max_topic_length configuration option can now be found in the
  serverinfo{} block

 -- Dominic Hargreaves <dom@earth.li>  Sun, 03 Feb 2013 00:17:41 +0000

ircd-hybrid (1:8.0.4.dfsg.1-1) experimental; urgency=low

  This version of ircd-hybrid includes some configuration file changes
  including a sid, which must be set uniquely within a network. If you
  link your server to others, make sure that you update this from the
  default configuration supplied by the Debian package.

 -- Dominic Hargreaves <dom@earth.li>  Sat, 12 Jan 2013 18:48:37 +0000
